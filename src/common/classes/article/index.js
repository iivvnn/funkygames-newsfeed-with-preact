export default class Article {
  constructor(title, description, link, guid, category, creator, pubDate, image) {
    this.title = title;
    this.description = description;
    this.link = link;
    this.guid = guid;
    this.category = category;
    this.creator = creator;
    this.pubDate = pubDate;
    this.image = image;
  }
}
