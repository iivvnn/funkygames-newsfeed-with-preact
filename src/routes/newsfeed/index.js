import { h, Component } from 'preact';
import 'preact-material-components/Card/style.css';
import 'preact-material-components/Button/style.css';
import style from './style';
import NewsFeedCard from '../../components/newsfeedcard';

export default class NewsFeed extends Component {
  render() {
    return (
      <div class={`${style.home} page`}>
        {this.props.news &&
          this.props.news.map(article => {
            return (
              <NewsFeedCard
                id={article.guid}
                title={article.title}
                description={article.description}
                link={article.link}
                category={article.category}
                creator={article.creator}
                pubDate={article.pubDate}
                image={article.image}
              />
            );
          })}
      </div>
    );
  }
}
