import React, { Component } from 'react';
import 'preact-material-components/Card/style.css';

import Button from 'preact-material-components/Button';
import 'preact-material-components/Button/style.css';
import style from './style';
import Card from 'preact-material-components/Card';
import LayoutGrid from 'preact-material-components/LayoutGrid';
import 'preact-material-components/LayoutGrid/style.css';

export default class NewsFeedCard extends Component {

  parseDate = (date) => {
    const NEW_DATE = new Date(Date.parse(date));
    const DAY_MONTH_YEAR = `${NEW_DATE.getDay()}.${NEW_DATE.getMonth()}.${NEW_DATE.getFullYear()}`;
    const HOURS_MIN = `${NEW_DATE.getHours()}:${NEW_DATE.getMinutes()}`;

    return `${DAY_MONTH_YEAR}, ${HOURS_MIN}`;
  };

  parseUrl = (url) => {
    return url.replace('https://beta.futurezone.de/', 'https://www.futurezone.de/');
  };

  render(){
    return (
      <Card class={style.home}>
        <LayoutGrid>
          <LayoutGrid.Inner class={style.gridInnerLeft}>
            <LayoutGrid.Cell cols="4">
              <div class={style.landscape}>
                <img src={this.props.image}/>
              </div>
            </LayoutGrid.Cell>

            <LayoutGrid.Cell cols="8" class={style.articleText}>
              <LayoutGrid class={style.gridRight}>
                <LayoutGrid.Inner>
                  <LayoutGrid.Cell cols="12">
                    <div class={style.cardHeader}>
                      <h2 class={style.headLine}>{this.props.title}</h2>
                      <div class={style.creatorTag}>
                        {this.props.creator} | {this.parseDate(this.props.pubDate)}
                      </div>
                      <div class={style.description}>{this.props.description}</div>
                      <div class={style.link}>
                        <a href={this.parseUrl(this.props.link)} target="_blank">
                          Weiter zum Artikel
                        </a>
                      </div>
                    </div>
                  </LayoutGrid.Cell>

                  <LayoutGrid.Cell col="12">
                    <div class={style.catWrapper}>
                      <Button class={style.catLabel} raised disabled>
                        {this.props.category}
                      </Button>
                    </div>
                  </LayoutGrid.Cell>
                </LayoutGrid.Inner>
              </LayoutGrid>
            </LayoutGrid.Cell>
          </LayoutGrid.Inner>
        </LayoutGrid>
      </Card>
    );
  }
};


