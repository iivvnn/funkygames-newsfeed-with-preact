import { h, Component } from 'preact';
import { Router } from 'preact-router';
import NewsFeed from '../routes/newsfeed';
import NotFound from '../routes/404';
import Article from '../common/classes/article';
import Header from '../components/header';
import logo from '../assets/funkygames-logo.jpeg';

export default class App extends Component {

  state = {
    newsFeed: {
      number: null,
      date: null,
      news: null,
    },
  };


  handleRoute = e => {
    this.setState({
      currentUrl: e.url,
    });
  };

  fetchRssFeed = () => {

    const proxyUrl = 'https://cors.io/?';
    const funkyGamesRssFeed = 'https://funkygames.de/rss.xml';
    const request = new Request(proxyUrl + funkyGamesRssFeed);

    return fetch(request).then(results => {
      results
        .text()
        .then(str => {
          let responseDocument = new DOMParser().parseFromString(str, 'application/xml');
          const newsItems = responseDocument.getElementsByTagName('item');

          let newsFeed = {
            number: newsItems.length,
            date: new Date(),
            news: [],
          };

          for (let i = 1; i < newsItems.length; i++) {
            let article = new Article(
              responseDocument.getElementsByTagName('title')[i].textContent,
              responseDocument.getElementsByTagName('description')[i].textContent,
              responseDocument.getElementsByTagName('link')[i].textContent,
              responseDocument.getElementsByTagName('guid')[i].textContent,
              responseDocument.getElementsByTagName('category')[i].textContent,
              responseDocument.getElementsByTagName('dc:creator')[i].textContent,
              responseDocument.getElementsByTagName('pubDate')[i].textContent,
              responseDocument.getElementsByTagName('enclosure')[i].attributes[0].nodeValue,
            );

            newsFeed.news.push(article);
          }

          this.setState({ newsFeed: newsFeed });
        })
        .catch(error => console.log(error));
    });
  };

  componentWillMount = () => {
    this.fetchRssFeed();
  };

  render() {
    return (
      <div id="app">
        <Header logo={logo}/>
        <Router onChange={this.handleRoute}>
          <NewsFeed news={this.state.newsFeed.news} path="/"/>
          <NotFound default/>
        </Router>
      </div>
    );

  };
}