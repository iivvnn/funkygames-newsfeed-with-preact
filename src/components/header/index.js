import { h, Component } from 'preact';

import TopAppBar from 'preact-material-components/TopAppBar';
import 'preact-material-components/TopAppBar/style.css';
import style from './style.css';

export default class Header extends Component {
  render(){
    return (
      <div >
        <TopAppBar fixed={true} class={style.topappbar}>
          <TopAppBar.Row>
            <TopAppBar.Section align-start >
              <div class={style.logoContainer}>
              <img class={style.logo} src={this.props.logo} alt="Funky Games Logo"/>
              </div>
              <TopAppBar.Title class={style.title}>
                Funky Games Newsfeed
              </TopAppBar.Title>
            </TopAppBar.Section>
          </TopAppBar.Row>
        </TopAppBar>
      </div>
    );
  }
}